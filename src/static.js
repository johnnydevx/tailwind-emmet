const staticUtilities = {
  '.W': { width: '100vw' },
  '.H': { height: '100vh' },
  '.F': { width: '100vw', height: '100vh' },
  '.w': { width: '100%' },
  '.h': { height: '100%' },

  '.n': { display: 'none' },
  '.b': { display: 'block' },
  '.f': { display: 'flex' },
  '.g': { display: 'grid' },

  '.mx': { marginLeft: 'auto', marginRight: 'auto' },
  '.ml': { marginLeft: 'auto' },
  '.mr': { marginRight: 'auto' },
  '.mt': { marginTop: 'auto' },
  '.mb': { marginBottom: 'auto' },

  /// flex-direction:
  '.c': { flexDirection: 'column' },
  '.cr': { flexDirection: 'column-reverse' },
  '.r': { flexDirection: 'row' },
  '.rr': { flexDirection: 'row-reverse' },

  /// align-items:
  '.a': { alignItems: 'start' },
  '.ac': { alignItems: 'center' },
  '.ae': { alignItems: 'end' },
  '.as': { alignItems: 'stretch' },

  /// justify-content:
  '.js': { justifyContent: 'start' },
  '.jc': { justifyContent: 'center' },
  '.je': { justifyContent: 'end' },
  '.jb': { justifyContent: 'space-between' },
  '.ja': { justifyContent: 'space-around' },

  '.fxs0': { flexShrink: 0 },

  // Position
  '.pos': { position: 'static' },
  '.por': { position: 'relative' },
  '.poa': { position: 'absolute' },
  '.pof': { position: 'fixed' },
  '.pot': { position: 'sticky' },

  // Cursor
  '.cup': { cursor: 'pointer' },

  // Misc
  '.usn': { userSelect: 'none' },
  '.bdrs': { borderRadius: '50%' },
  '.tac': { textAlign: 'center' },
  '.ovh': { overflow: 'hidden' },
  '.an': { appearance: 'none' },
};

module.exports = staticUtilities;
