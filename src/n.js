const n = {
  to2048: {},
  to512: {},
  to256: {},
  to32: {},
};

for (let i = 0; i <= 2048; i++) n.to2048[i] = i;
for (let i = 0; i <= 512; i++) n.to512[i] = i;
for (let i = 0; i <= 256; i++) n.to256[i] = i;
for (let i = 0; i <= 32; i++) n.to32[i] = i;

module.exports = n;
