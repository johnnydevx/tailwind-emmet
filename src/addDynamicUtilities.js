const toColorValue = require("./utils/toColorValue");
const flattenColorPalette = require("./utils/flattenColorPalette");
const n = require("./n");

const addDynamicUtilities = ({ theme, matchUtilities }) => {
  matchUtilities({
    bgi: v => ({ backgroundImage: `url('/public/assets/${v}')` }),
    bgc: v => ({ backgroundColor: toColorValue(v) }),

    // { values: flattenColorPalette(theme('textColor')), type: 'color' }
    // For use cases when we need both background image and linear-gradient under the background-image prop
    // bgig-[linear-gradient(45deg, #000, #0009), url('/public/assets/${v}')]
    bgig: v => ({ backgroundImage: v }),
    bgs: v => ({ backgroundSize: `${v}` }),
    gc: v => ({ gridTemplateColumns: `repeat(${v}, 1fr)` }),
  });

  matchUtilities(
    {
      fz: n => ({ fontSize: n }),
    },
    { values: n.to256 }
  );

  matchUtilities(
    {
      fx: n => ({ flex: n }),
    },
    { values: n.to32 }
  );

  matchUtilities(
    {
      w: n => ({ width: n }),
      h: n => ({ height: n }),
      t: n => ({ top: n }),
      r: n => ({ right: n }),
      b: n => ({ bottom: n }),
      l: n => ({ left: n }),
      z: n => ({ zIndex: n }),
      p: n => ({ padding: n }),
      px: n => ({ paddingLeft: n, paddingRight: n }),
      py: n => ({ paddingTop: n, paddingBottom: n }),
      pl: n => ({ paddingLeft: n }),
      pr: n => ({ paddingRight: n }),
      pt: n => ({ paddingTop: n }),
      pb: n => ({ paddingBottom: n }),
      m: n => ({ margin: n }),
      mx: n => ({ marginLeft: n, marginRight: n }),
      my: n => ({ marginTop: n, marginBottom: n }),
      ml: n => ({ marginLeft: n }),
      mr: n => ({ marginRight: n }),
      mt: n => ({ marginTop: n }),
      mb: n => ({ marginBottom: n }),
      bdrs: n => ({ borderRadius: n }),
      g: n => ({ gap: n }),
      maw: n => ({ maxWidth: n }),
      miw: n => ({ minWidth: n }),
      mih: n => ({ minHeight: n }),
      mah: n => ({ maxHeight: n }),
      lh: n => ({ lineHeight: n }),
      rect: n => ({
        width: n,
        height: n,
        flexShrink: 0,
      }),
    },
    { values: n.to2048 }
  );

  matchUtilities(
    { fw: v => ({ fontWeight: v }) },
    { values: theme("fontWeight") }
  );

  matchUtilities({ ls: v => ({ letterSpacing: v }) });

  matchUtilities(
    { c: v => ({ color: toColorValue(v) }) },
    { values: flattenColorPalette(theme("textColor")), type: "color" }
  );
};

module.exports = addDynamicUtilities;
