const plugin = require('tailwindcss/plugin');
const staticUtilities = require('./src/static');
const addDynamicUtilities = require('./src/addDynamicUtilities');

const TailwindEmmetPlugin = plugin(
  ({
    addUtilities, // static utility styles
    matchUtilities, // dynamic utility styles
    addComponents, // new static component styles
    matchComponents, // dynamic component styles
    theme, // looking up values in the user's theme configuration
    addBase, // registering new base styles 
    addVariant, // registering custom variants
    config, // looking up values in the user's Tailwind configuration
    corePlugins, // checking if a core plugin is enabled
    e, // manually escaping strings meant to be used in class names
    prefix, // ???
  }) => {
    addUtilities(staticUtilities);
    addDynamicUtilities({ theme, matchUtilities });
  }
);

module.exports = TailwindEmmetPlugin;
