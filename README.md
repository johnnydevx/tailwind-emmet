# tailwind-emmet
Tailwind 3 Emmet static and dynamic mappings plugin.

## Installation

### Package
`npm install tailwind-emmet --dev`

## Plugin
```js
// tailwind.config.js
{
  // ...
  "plugins": [
    // ...
    require("tailwind-emmet"),
    // ...
  ],
  // ...
}
```